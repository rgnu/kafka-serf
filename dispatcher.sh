#!/bin/sh

# Dispatch an event to the appropriate handlers.
#
# $ tree /etc/serf
# /etc/serf
# └── handlers
#     ├── member-failed
#     ├── member-join
#     ├── member-leave
#     ├── member-update
#     ├── member
#     ├── user-deploy
#     ├── user
#     ├── query-uptime
#     └── query
#         └── role
#
# All the scripts in the matching directories will be executed. If there
# is a directory matching the role of the received event, all the
# scripts in this directory will be executed as well.

if [ "$SERF_EVENT" = "user" ]; then
    EVENT="user-$SERF_USER_EVENT"
elif [ "$SERF_EVENT" = "query" ]; then
    EVENT="query-$SERF_QUERY_NAME"
else
    EVENT=$SERF_EVENT
fi

export SERFLOG="${SERFLOG:-/dev/stdout}"
export SERFDIR="${SERFDIR:-/etc/serf}"
export ROLE=${SERF_TAG_ROLE:-${SERF_SELF_ROLE:-default}}
export EVENT=$EVENT
export PAYLOAD="$(cat)"

while true; do
    HANDLER="$SERFDIR/handlers/$EVENT"
    [ -d "$HANDLER" ] && {
        [ -z "$VERBOSE" ] || echo "[+] Handle $EVENT" >> ${SERFLOG}
        run-parts "$HANDLER"
    }
    [ -d "$HANDLER/$ROLE" ] && {
        [ -z "$VERBOSE" ] || echo "[+] Handle $EVENT for role $ROLE" >> ${SERFLOG}
        run-parts "$HANDLER/$ROLE"
    }
    [ "${EVENT%-*}" != "$EVENT" ] || break
    EVENT="${EVENT%-*}"
done
