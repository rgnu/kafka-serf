FROM spotify/kafka
MAINTAINER OLX Team

COPY serf /usr/local/bin/serf
COPY jq /usr/local/bin/jq
COPY dispatcher.sh /etc/serf/dispatcher.sh
COPY handlers/ /etc/serf/handlers/
COPY supervisor/ /etc/supervisor/conf.d/

COPY start-kafka.sh /usr/bin/start-kafka.sh
COPY Dockerfile /Dockerfile
